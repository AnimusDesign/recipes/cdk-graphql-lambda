import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import {useGetUserQuery, useGetUsersQuery} from "../api/generated/graphql/generated/types";
import {UserTable} from "../components/user";

const Home: NextPage = () => {
  const { data, error, loading} = useGetUsersQuery({})
  if (loading) {
    return (<div>Loading</div>)
    
  } 
  if (error) {
    return (<div> Encountered Error See Console.</div>)
  }
  return (
    <div className={styles.container}>
      <UserTable users={data} />
    </div>
  )
}

export default Home
