import {NextPage} from "next";
import {useRouter} from "next/router";
import {useCreateNewUserMutation, useGetUserByRefCodeQuery} from "../api/generated/graphql/generated/types";
import {PropsWithChildren, useState} from "react";
import {router} from "next/client";

const SignUp: NextPage = () => {
  const router = useRouter()
  const refCodePresent = Object.keys(router.query).includes("refCode")
  let element: JSX.Element
  switch (refCodePresent) {
    case true:
      element = (<SignUpViaRef/>)
      break;
    case false:
      element = (<SignUpNoRef/>)
      break
  }
  return element
}

interface ISingUpFormProps extends React.Props<unknown> {
  refUser: string|null
}

const SignUpForm: React.FC<ISingUpFormProps> = (props:PropsWithChildren<ISingUpFormProps>) => {
  const router = useRouter()
  const [firstName, setFirstName] = useState<string>("")
  const [lastName, setLastName] = useState<string>("")
  const [email, setEmail] = useState<string>("")
  
  const  [mutateFunction, { data, loading, error }] = useCreateNewUserMutation()
  
  const handleClick = () => {
    const possibleRef = (props.refUser) ? {refUser: props.refUser} : {}
    mutateFunction({
      variables: {
        firstName,
        lastName,
        email,
        ...possibleRef
      }
    }).then(() => router.push({
      pathname: "/"
    }))
      .catch( (err) => console.error(err))
  }
  if (loading) {
    return (
      <span>Creating your user</span>
    )
  }
  if (error) {
    return (
      <span>Failed to add your user</span>
    )
  }
  if (data !== null && data !== undefined && data.createUser.id) {
    useRouter().push({
      pathname: "/"
    })
  }
  return (<div>
      <form>
        <label htmlFor="firstName">First Name</label>
        <input id="firstName" type="text" onChange={(event) => setFirstName(event.target.value)} value={firstName}/>
        <br/>
        <label htmlFor="firstName">Last Name</label>
        <input id="firstName" type="text" onChange={(event) => setLastName(event.target.value)} value={lastName}/>
        <br/>
        <label htmlFor="email">Email</label>
        <input id="email" type="email" onChange={(event) => setEmail(event.target.value)} value={email}/>
      </form>
      <button onClick={(event) => handleClick()}>Create Your User</button>
    </div>)
}

const SignUpViaRef: React.FC = (props: PropsWithChildren<unknown>) => {
  const {refCode} = useRouter().query
  const {data, error, loading} = useGetUserByRefCodeQuery({
    variables: {
      // TODO: Sanitize this
      refCode: refCode as string
    }
  })
  if (loading) {
    return (<div>Loading your friend</div>)
  }
  if (error) {
    return (<div>

        <span>"Failed to pull up your friend"</span>
        <span>"You can try without the Ref Code"</span>
      </div>)
  }
  return (<div>
      Sign Up
      <hr/>
      <SignUpForm refUser={data?.getUserByRefCode.id ?? null}/>
    </div>)
}

const SignUpNoRef: React.FC = () => {
  return (<div>
      Sign Up
      <hr/>
      <SignUpForm refUser={null}/>
    </div>)
}
export default SignUp