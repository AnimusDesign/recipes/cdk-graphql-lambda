import {gql} from "@apollo/client";

export const UserMutations = gql`
  mutation createNewUser(
    $firstName: String!,
    $lastName: String!,
    $email: String!,
    $refUser: ID
  ) {
    createUser(
      firstName: $firstName,
      lastName: $lastName,
      email: $email
      referredBy: $refUser
    ) {
      id
      firstName
      lastName
      email
    }
  }
  
  mutation createRefCode($userId:ID!) {
    createUserReferralCode(id: $userId) 
  }
`