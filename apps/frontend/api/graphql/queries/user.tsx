import {gql} from '@apollo/client';

const GetUser = gql`
  query getUser($id: ID!) {
    getUser(id: $id) {
      id
      firstName
      lastName
      email
    }
  }

  query getUsers {
    getUsers {
      users {
        id
        email
        firstName
        lastName
        referredBy
        referralCode
        getReferrals {
          commission {
            id
            paid
          }
          referred
        }
      }
    }
  }

  query getUserByRefCode($refCode: String!) {
    getUserByRefCode(refCode: $refCode) {
      id
      firstName
      lastName
      getReferrals {
        commission {
          id
        }
      }
    }
  }
`