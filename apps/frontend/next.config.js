/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true, async rewrites() {
    let graphqlTarget = ""
    if (Object.keys(process.env).includes("LOCAL") && process.env["LOCAL"] !== undefined && process.env["LOCAL"] !== null) {
      console.log("Using local development graphql server.");
      graphqlTarget = "http://localhost:3000/graphql"
    } else {
      console.log("Using AWS API Gateway instance")
      // TODO: Use CDK or SSM Param
      graphqlTarget = "https://q2yyvuvmv9.execute-api.us-east-2.amazonaws.com/demo-backend-gateway/graphql" 
    }
    return [
      
      {
        source: "/graphql",
        destination: graphqlTarget
      }
    ]
  }
}

module.exports = nextConfig
