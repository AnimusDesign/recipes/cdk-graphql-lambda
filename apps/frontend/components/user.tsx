import {User} from "@demo/schemas-graphql/dist/generated/types";
import {PropsWithChildren, useState} from "react";
import {GetUsersQuery, useCreateRefCodeMutation} from "../api/generated/graphql/generated/types";
import Link from 'next/link'

export interface IUserTableProps extends React.Props<any> {
  users: GetUsersQuery
}

export const UserTable: React.FC<IUserTableProps> = (props: PropsWithChildren<IUserTableProps>) => {

  const rows = props.users.getUsers.users?.map((user) => {
    // TODO: CleanThis up repeated creation of function
    const [mutateFunction, {data, loading, error}] = useCreateRefCodeMutation()
    const [refCode, setRefCode] = useState<string | null | undefined>(user.referralCode)
    const referallsCount = user.getReferrals.referred.length
    const referalls = (referallsCount > 0) ? referallsCount : "No One Referred"

    const getCommissionCount = (status: boolean) => {
      return user.getReferrals.commission.filter((commission) => commission.paid == status)
    }

    const openCommissionCount = getCommissionCount(false)
    const openCommission = (openCommissionCount.length > 0) ? openCommissionCount.length : "No Open Commission"

    const closedCommissionCount = getCommissionCount(true)
    const closedCommission = (closedCommissionCount.length > 0) ? closedCommissionCount.length : "No Paid Commission"

    const referredBy = props.users.getUsers.users?.find((refUser) => refUser.id === user.referredBy)

    const refCodeElement: JSX.Element = (refCode) ? (<Link href={`/signUp?refCode=${refCode}`}>
      {refCode}
    </Link>) : (<button onClick={() => {
      mutateFunction({
        variables: {userId: user.id}
      }).then((rsp) => setRefCode(rsp.data?.createUserReferralCode))
    }}> Create Referral Code</button>)

    return (<tr id={user.id} key={user.id}>
      <td>{user.firstName}</td>
      <td>{user.lastName}</td>
      <td>{referalls}</td>
      <td>{openCommission}</td>
      <td>{closedCommission}</td>
      <td>{(referredBy) ? `${referredBy.firstName} ${referredBy.lastName}` : ""}</td>
      <td>{refCodeElement}</td>
    </tr>)
  }) ?? []
  return (<table>
    <thead>
    <tr>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Referrals</th>
      <th>Open Commission</th>
      <th>Closed Commission</th>
      <th>Referred By</th>
      <th>Ref Code</th>
    </tr>
    </thead>
    <tbody>
    {rows}
    </tbody>
  </table>)
} 