import {makeExecutableSchema} from "@graphql-tools/schema"
import schema from "@demo/schemas-graphql"
import {ApolloServerPluginLandingPageGraphQLPlayground} from "apollo-server-core"
import type {Config} from "apollo-server-lambda"
import {resolvers as scalarTypeResolvers} from "graphql-scalars"
import {IContext} from "./records";
import {NestedUserQueries, Resolvers} from "./queries";
import {DynamoDBClient} from "@aws-sdk/client-dynamodb";
import {Mutations} from "./mutations";

const {DateTime, Currency, EmailAddress, JSON, URL, NonEmptyString} = scalarTypeResolvers

export const config: Config = {
  schema: makeExecutableSchema<IContext>({
    typeDefs: [...schema], 
    resolvers: {
      DateTime, Currency, 
      EmailAddress, 
      JSON, 
      URL, 
      NonEmptyString,
      Query: Resolvers,
      Mutation: Mutations,
      User: NestedUserQueries
    }
  }), 
  context: async (input) => {
    const dynamoClient =    new DynamoDBClient({ region: "us-east-2" });
    return {
      dynamoClient: dynamoClient
    } as IContext
  },
  plugins: [ApolloServerPluginLandingPageGraphQLPlayground()]
}
