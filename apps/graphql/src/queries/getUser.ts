import {
  QueryGetUserArgs, QueryGetUserByRefCodeArgs,
  QueryResolvers, Referral,
  ResolversParentTypes,
  User,
  UserList, UserResolvers
} from "@demo/schemas-graphql/dist/generated/types";
import {IContext} from "../records";
import {GraphQLResolveInfo} from "graphql";
import {getUser as getDynamoUser} from "../persistence"
import {Table} from "dynamo-light";
import {GetItemOutput, ScanOutput} from "@aws-sdk/client-dynamodb";


export const getUser: QueryResolvers["getUser"] = async (
  parent: ResolversParentTypes["Query"], args: QueryGetUserArgs, context: IContext, info: GraphQLResolveInfo): Promise<User> => {
  return await getDynamoUser(context.dynamoClient, args.id)
}

export const getUserReferrals : UserResolvers["getReferrals"] = async (
  parent: ResolversParentTypes["User"], args: {}, context: IContext, info: GraphQLResolveInfo): Promise<Referral> => {
  const referralTable = new Table("referral")
  const rsp = await referralTable.get(parent.id) as GetItemOutput
  console.log(JSON.stringify(rsp, null, 4))
  return rsp.Item as unknown as Referral
}

export const getUserByRefCode : QueryResolvers["getUserByRefCode"] = async (
  parent: ResolversParentTypes["Query"], args: QueryGetUserByRefCodeArgs, context: IContext, info: GraphQLResolveInfo): Promise<User> => {
  // TODO: Make Table Name a Constant
  // TODO: Add Secondary index to avoic scan
  const usersTable = new Table("users")
  const possibleUserScan : ScanOutput = (await usersTable.scan())
  
  // @ts-ignore
  const possibleUser = possibleUserScan.Items?.filter( (user) => user.referralCode === args.refCode) ?? []
  
  if (possibleUser.length === null || possibleUser.length === undefined || possibleUser.length === 0 ) {
    throw Error("Could not find a user by that code.")
  }
  if (possibleUser.length > 1) {
    throw Error("Found multiple users with that ref code")
  }
  return possibleUser[0] as unknown as User
}
