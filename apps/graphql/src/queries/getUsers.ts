import {
  QueryGetUserArgs,
  QueryResolvers,
  ResolversParentTypes,
  User,
  UserList
} from "@demo/schemas-graphql/dist/generated/types";
import {IContext} from "../records";
import {GraphQLResolveInfo} from "graphql";
import {Table} from "dynamo-light";
import {ScanOutput} from "@aws-sdk/client-dynamodb";

export const getUsers: QueryResolvers["getUsers"] = async (
  parent: ResolversParentTypes["Query"], args: {  }, context: IContext, info: GraphQLResolveInfo): Promise<UserList> => {
  const users = new Table("users")
  const usersScan: ScanOutput = await users.scan()
  return {
    users: usersScan.Items as unknown as User[]
  }
}