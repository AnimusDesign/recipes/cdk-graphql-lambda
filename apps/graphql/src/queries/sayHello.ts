import {
  HelloResponse, QueryResolvers, QuerySayHelloArgs, ResolversParentTypes
} from "@demo/schemas-graphql/dist/generated/types";
import {IContext} from "../records";
import {GraphQLResolveInfo} from "graphql";

export const sayHello: QueryResolvers["sayHello"] = async (parent: ResolversParentTypes["Query"], args: QuerySayHelloArgs, context: IContext, info: GraphQLResolveInfo): Promise<HelloResponse> => {
  return {
    message: `Hello ${args.name}`
  }
}