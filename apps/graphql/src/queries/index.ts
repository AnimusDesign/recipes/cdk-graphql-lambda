import {QueryResolvers, UserResolvers} from "@demo/schemas-graphql/dist/generated/types";
import {IContext} from "../records";
import {sayHello} from "./sayHello";
import {getUsers} from "./getUsers";
import {getUser, getUserByRefCode, getUserReferrals} from "./getUser";

export const Resolvers: QueryResolvers<IContext> = {
  sayHello, getUsers, getUser, getUserByRefCode
}

export const NestedUserQueries : UserResolvers<IContext> = {
  getReferrals: getUserReferrals
}