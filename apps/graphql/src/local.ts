import {ApolloServer, Config} from "apollo-server-express"
import Express from "express"
import {config} from "./config"


async function launchLocalServer() {
  console.log("========================")
  console.log("Launching Local Development Server")
  console.log("========================")
  const localConfig: Config = {
    ...config
  }
  const server = new ApolloServer(localConfig)
  await server.start()

  const app = Express()
  server.applyMiddleware({app})

  const host = "0.0.0.0"
  const port = 3000
  type ResolveFunc = () => void
  await new Promise((resolve) => app.listen({
    host: host, port: port
  }, resolve as ResolveFunc))
  console.log(`🚀 Server ready at http://${host}:${port}${server.graphqlPath}`)
  return {server, app}
}

launchLocalServer()
  .then(() => console.log("Running"))
  .catch((err) => console.error(err))
