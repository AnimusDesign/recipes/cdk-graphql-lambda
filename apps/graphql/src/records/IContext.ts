import {DynamoDBClient} from "@aws-sdk/client-dynamodb";

export interface IContext {
  dynamoClient: DynamoDBClient
}
