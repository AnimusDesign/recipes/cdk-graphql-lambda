import {DynamoDBClient, PutItemCommand, UpdateItemCommand, UpdateItemCommandInput} from "@aws-sdk/client-dynamodb";
import { marshall, unmarshall } from "@aws-sdk/util-dynamodb"
import { ResponseMetadata } from "@aws-sdk/types"

const marshallOptions = { convertClassInstanceToMap: true }

export function isBadHttpResponse(metadata: ResponseMetadata): boolean {
  return (metadata.httpStatusCode ?? 300) > 299
}

export async function create<R>(
  client: DynamoDBClient,
  table: string,
  record: R,
): Promise<R> {
    const putOpts = {
      TableName: table,
      Item: marshall(record,         marshallOptions      )
    }
    console.log(JSON.stringify(putOpts))
    const input = new PutItemCommand(putOpts)
    const rsp = await client.send(input)
    if (isBadHttpResponse(rsp.$metadata)) {
      throw Error(`Failed to create on ${table}`)
    }
    return record
  
}

export async function update<R>(
  client:DynamoDBClient,
  table: string,
  key:string,
  updateExpression: string,
  values: any
) : Promise<boolean> {
  const updateOpts = {
    TableName: table,
    Key: marshall({"id": key}),
    UpdateExpression: updateExpression,
    ExpressionAttributeValues: marshall(values)
  } as UpdateItemCommandInput
  console.log(JSON.stringify(updateOpts, null, 4))
  const updateCommand =new UpdateItemCommand(updateOpts)
  const rsp = await client.send(updateCommand)
  if (isBadHttpResponse(rsp.$metadata)) {
    throw Error(`Failed to create on ${table}`)
  }
  return true
}

export * from "./getUser";