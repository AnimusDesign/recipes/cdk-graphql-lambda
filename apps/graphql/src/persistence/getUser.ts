import {DynamoDBClient, GetItemOutput, ScanCommandOutput} from "@aws-sdk/client-dynamodb";
import {Table} from "dynamo-light";
import {User} from "@demo/schemas-graphql/dist/generated/types";

export async function getUser(client:DynamoDBClient, id:string) : Promise<User> {
  const users = new Table("users")
  const getUserResult: GetItemOutput = await users.get(id)
  return getUserResult.Item as unknown as User
}

export async function userEmailTaken(email:string) : Promise<boolean> {
  const users = new Table("users")
  const getUserResult = (await users.scan()).Items ?? []
  return (getUserResult.filter( (user:User) => user.email === email)) > 0
}