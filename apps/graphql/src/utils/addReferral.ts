import {Table} from "dynamo-light";
import {getUser as getDynamoUser, update} from "../persistence"
import {DynamoDBClient, GetItemOutput} from "@aws-sdk/client-dynamodb";
import {Referral, User, UserReferral} from "@demo/schemas-graphql/dist/generated/types";
import {Constants} from "../records";

export async function addReferral(client: DynamoDBClient, refereeId: string, newUser: User) {
  console.log(" In add referral.")
  const referral = new Table("referral")
  
  const referee = await getDynamoUser(client, refereeId)
  
  const possibleReferrals  = (await referral.get(refereeId)).Item as Referral|null|undefined
  // @ts-ignore
  let updatedReferralRecord: Omit<Referral, "id"> = {}
  if (possibleReferrals === null || possibleReferrals === undefined) {
    updatedReferralRecord = {...(await initReferral(client, refereeId))}
  } else {
    updatedReferralRecord = {...possibleReferrals}
  }
  
  if (updatedReferralRecord.commission.length < Constants.maxReferrals) {
    updatedReferralRecord.commission.push({
      id: newUser.id,
      paid: false,
      claimed: new Date().toISOString()
    } as UserReferral)
  }
  if (!updatedReferralRecord.referred.includes(newUser.id)) {
    updatedReferralRecord.referred.push(newUser.id)
  }
  //TODO: Fix this type error
  // @ts-ignore
  delete updatedReferralRecord.id
  await referral.update(refereeId, updatedReferralRecord)

}

export async function initReferral(client:DynamoDBClient, refereeId: string) : Promise<Referral> {
  const referral = new Table("referral")
  const record = {
    id: refereeId,
    referred: [],
    commission: []
  } as Referral
  await referral.put(record)
  return record
}