import {ApolloServer} from "apollo-server-lambda"
import type {Callback, Context} from "aws-lambda"

import {config} from "./config"

exports.handler = async (
  event: {},
  context: Context,
  callback: Callback
) => {
  const server = new ApolloServer(config)
  return server.createHandler({})(event, context, callback)
}