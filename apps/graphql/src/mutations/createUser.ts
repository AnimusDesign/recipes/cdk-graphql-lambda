import {
  MutationCreateUserArgs,
  MutationResolvers,
  ResolversParentTypes,
  User
} from "@demo/schemas-graphql/dist/generated/types";
import {IContext} from "../records";
import {GraphQLResolveInfo} from "graphql";
import {v4 as uuidV4} from "uuid"
import {create, userEmailTaken} from "../persistence";
import {addReferral, initReferral} from "../utils";

export const createUser: MutationResolvers["createUser"] = async (parent: ResolversParentTypes["Mutation"], args: MutationCreateUserArgs, context: IContext, info: GraphQLResolveInfo): Promise<User> => {
  const userId = uuidV4()
  if (await userEmailTaken(args.email)) {
    throw Error(`User email: ${args.email} already in use.`)
  }
  const userRecord = {
    id: userId,
    firstName: args.firstName,
    lastName: args.email,
    email: args.email
  } as User
  
  if (args.referredBy !== null && args.referredBy !== undefined) {
    userRecord["referredBy"] = args.referredBy as string
  }
  await create(context.dynamoClient, "users", userRecord)
  if (args.referredBy !== null && args.referredBy !== undefined) {
    await addReferral(context.dynamoClient, args.referredBy, userRecord)
  }
  await initReferral(context.dynamoClient, userRecord.id)
  return userRecord
}
