import {
  MutationCreateUserReferralCodeArgs,
  MutationResolvers,
  ResolversParentTypes,
  User
} from "@demo/schemas-graphql/dist/generated/types";
import {IContext} from "../records";
import {GraphQLResolveInfo} from "graphql";
import {v4 as uuidV4} from "uuid"
import {create, update} from "../persistence";
import {createRandomString, initReferral} from "../utils";

export const createUserReferralCode: MutationResolvers["createUserReferralCode"] = async (
  parent: ResolversParentTypes["Mutation"], args: MutationCreateUserReferralCodeArgs, context: IContext, info: GraphQLResolveInfo): Promise<string> => {
  const referralCode = createRandomString(10)
  await update(context.dynamoClient, "users", args.id,
    "SET referralCode = :r", {
      ":r": referralCode
    })
  await initReferral(context.dynamoClient, args.id)
  return referralCode
}