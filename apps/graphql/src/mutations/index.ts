import {IContext} from "../records";
import {MutationResolvers} from "@demo/schemas-graphql/dist/generated/types";
import {createUser} from "./createUser";
import {createUserReferralCode} from "./createUserReferralCode";

export const Mutations : MutationResolvers<IContext> = {
  createUser: createUser,
  createUserReferralCode: createUserReferralCode,
}