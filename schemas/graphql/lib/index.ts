import {HelloResponse, Referral, User} from "./records";
import {ReferralQueries, sayHello, UserQueries} from "./queries";
import gql from "graphql-tag";
import {ReferralMutations} from "./mutations/referral";
import {UserMutations} from "./mutations";

export * from "./queries";
export * from "./records";

export default [
  // Root Configuration
  gql`
    scalar EmailAddress
    scalar Currency
    scalar DateTime
    scalar JSON
    scalar URL
    scalar NonEmptyString
    type Query
    type Mutation
  `,
  // Objects
  HelloResponse,
  Referral,
  User,
  // Queries
  sayHello,
  UserQueries,
  ReferralQueries,
  // Mutations
  UserMutations,
  ReferralMutations
  
]