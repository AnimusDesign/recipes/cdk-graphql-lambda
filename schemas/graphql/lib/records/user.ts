import gql from "graphql-tag";

export const User = gql`
    type User {
      id: ID!,
      firstName: String!,
      lastName: String!,
      email:String!,
      referralCode: String,
      referredBy: ID
    } 
    
    type UserList {
      users: [User!]
    }
`