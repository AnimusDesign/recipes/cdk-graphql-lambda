import gql from "graphql-tag";

export const HelloResponse = gql`
  type HelloResponse {
    message: String!
  }
`

export * from "./referral"
export * from "./user"