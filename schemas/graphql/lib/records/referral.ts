import gql from "graphql-tag";

export const Referral = gql`
    type UserReferral {
      id: ID!,
      paid: Boolean!,
      claimed: DateTime
    }  
    type Referral {
      id: String!,
      referred: [ID!]!,
      commission: [UserReferral!]!
    }
    
    type ReferralResponse {
      code: String!,
      remaining: Int!
    }
`