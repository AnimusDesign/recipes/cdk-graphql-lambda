import gql from "graphql-tag";

export const ReferralMutations = gql`
  extend type Mutation  {
    logReferral(referralCode: String!, newUser:ID!) : Referral
  }
`