import gql from "graphql-tag";

export const UserMutations = gql`
  extend type Mutation {
    createUser(email:String!, firstName:String!, lastName:String!, referredBy:ID) : User!
    createUserReferralCode(id:ID!) : ID!
  }
`