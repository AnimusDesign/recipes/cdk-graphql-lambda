import gql from "graphql-tag";

export const sayHello = gql`
  extend type Query {
    sayHello(name: String!) : HelloResponse
    
  }
`

export * from "./user"
export * from "./referral"