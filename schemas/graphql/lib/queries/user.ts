import gql from "graphql-tag";

export const UserQueries = gql`
  extend type Query  {
    getReferralCode(id: ID!) : ReferralResponse
    
    getUsers : UserList!
    
    getUser(id:ID!) : User!

    getUserByRefCode(refCode: String!) : User!
  }
  
  extend type User {
    getReferrals : Referral!
    
  }
`
