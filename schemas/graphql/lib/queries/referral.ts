import gql from "graphql-tag";

export const ReferralQueries = gql`
  extend type Query {
    getReferral(id:ID!) : Referral!
    getUserReferrals(userId: ID!) : Referral!
  }
`