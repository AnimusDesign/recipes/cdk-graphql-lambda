import {aws_cloudfront, Stack, StackProps} from "aws-cdk-lib";
import {Construct} from "constructs";
import {NextjsArtifact, ServerlessNextjs} from "@rayova/cdk-serverless-nextjs";
import * as path from "path";

export class Frontend extends Stack {
  constructor(scope:Construct, id:string, props: StackProps) {
    super(scope, id, props);
    const frontendRoot = path.join(__dirname, "../../../apps/frontend")
    const serverlessNextjs = new ServerlessNextjs(scope, 'NextJs', {
      // Then produce and add a Next.js artifact to ServerlessNextjs by
      // building it from your project directory.
      nextjsArtifact: NextjsArtifact.fromBuild({
        // Provide your Next.js project directory.
        nextjsDirectory: frontendRoot,
        // Provide the commands you need NextjsArtifact to run to build the
        // `.next` directory.
        buildCommand: ['yarn', 'next', 'build'],
      }),
    });

// Create CloudFront distribution
    const dist = new aws_cloudfront.Distribution(scope, 'Distribution', {
      // And configure it with serverless Next.js as Lambda @ Edge
      ...serverlessNextjs.cloudFrontConfig,

      // Add anything else you need on your CloudFront distribution here, like
      // certificates, cnames, price classes, logging, etc.
    });
  }
}