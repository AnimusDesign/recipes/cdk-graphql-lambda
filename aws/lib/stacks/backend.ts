import {Stack, StackProps} from "aws-cdk-lib";
import {Construct} from "constructs";
import {createAPIGateway, createBackendVPC, createGraphQLLambda} from "../backend";
import {createDynamoDB} from "../backend/DynamoDB";


export class BackendStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);
    
    const vpc = createBackendVPC(this)
    const apiGateway = createAPIGateway(this)
    
    const tables = createDynamoDB(this)
    
    const graphQLLambda = createGraphQLLambda(this, vpc, apiGateway, tables)
    
  }
}