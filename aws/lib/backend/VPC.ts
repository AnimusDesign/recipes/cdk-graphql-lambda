import {Construct} from "constructs";
import {SubnetType, Vpc} from "aws-cdk-lib/aws-ec2";
import {RemovalPolicy} from "aws-cdk-lib";

export function createBackendVPC(
  stack:Construct
) : Vpc {
  const vpcName = "demo-vpc"
  const vpc = new Vpc(
    stack, vpcName, {
      maxAzs: 1,
      subnetConfiguration: [
        {
          name: "demo-vpc-public",
          subnetType: SubnetType.PUBLIC
        },
        {
          name: "demo-vpc-private",
          subnetType: SubnetType.PRIVATE_WITH_NAT
        }
      ]
    }
  )
  vpc.applyRemovalPolicy(RemovalPolicy.DESTROY)
  return vpc
}