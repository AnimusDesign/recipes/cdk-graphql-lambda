import {Construct} from "constructs";
import {MockIntegration, Model, PassthroughBehavior, RestApi} from "aws-cdk-lib/aws-apigateway";

export function createAPIGateway(
  stack: Construct
): RestApi {
  const gatewayName = "demo-backend-gateway"
  return new RestApi(stack, gatewayName, {
    restApiName: gatewayName,
    deployOptions: {
      stageName: gatewayName
    },
    binaryMediaTypes: [
      "image/png",
      "image/jpeg",
      "image/webp",
      "image/svg+xml"
    ],
    defaultIntegration: new MockIntegration({
      passthroughBehavior: PassthroughBehavior.WHEN_NO_TEMPLATES,
      requestTemplates: {
        "application/json": JSON.stringify({
          statusCode: 200
        })
      },
      integrationResponses: [
        {
          statusCode: "503",
          responseTemplates: {
            "application/json": JSON.stringify({
              message: "Under Maintenance"
            })
          }
        }
      ]
    }),
    defaultMethodOptions: {
      methodResponses: [
        {
          statusCode: "503",
          responseModels: {
            "application/json": Model.ERROR_MODEL
          }
        }
      ]
    }
  })
}