import {Construct} from "constructs";

import {NodejsFunction} from "aws-cdk-lib/aws-lambda-nodejs";
import * as path from "path";
import {aws_iam, Duration} from "aws-cdk-lib";
import {IVpc} from "aws-cdk-lib/aws-ec2";
import {LambdaIntegration, RestApi} from "aws-cdk-lib/aws-apigateway";
import {Table} from "aws-cdk-lib/aws-dynamodb";

export function createGraphQLLambda(stack:Construct, 
                                    vpc: IVpc,
                                    gateway: RestApi,
                                    tables:Table[]
                                    ) : NodejsFunction {
  const graphQLRootDir = path.join(__dirname, "../../../apps/graphql/")
  const graphQLHandler = new NodejsFunction(
    stack,
    "demo-graphql",
    {
      entry: path.join(graphQLRootDir, "src/lambda.ts"),
      handler: "handler",
      memorySize: 128,
      environment: {},
      timeout: Duration.seconds(5),
      vpc: vpc,
      bundling: {
        minify: true,
        nodeModules: []
      }
    }
  )
  
  
  const apiGatewayPolicy = new aws_iam.PolicyStatement({
    actions: ["apigateway:*"],
    resources: [
      `arn:aws:apigateway:us-east-2::/apikeys`,
      `arn:aws:apigateway:us-east-2::/tags/*`
    ]
  })
  
  const tablePolicy = new aws_iam.PolicyStatement({
    actions: [
      "dynamodb:*"
    ],
    resources: tables.map( (table) => table.tableArn)
  })

  graphQLHandler.role?.attachInlinePolicy(
    new aws_iam.Policy(stack, "graphql-handler-gateway", {
      statements: [
        apiGatewayPolicy,
        tablePolicy
      ]
    })
  )
  
  const gatewayGraphQL = gateway.root.addResource("graphql")
  gatewayGraphQL.addProxy()
  gatewayGraphQL.addMethod("ANY", new LambdaIntegration(graphQLHandler))
  
  return graphQLHandler
}