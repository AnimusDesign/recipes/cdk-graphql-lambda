import {Construct} from "constructs";
import {AttributeType, Table} from "aws-cdk-lib/aws-dynamodb";
import {create} from "domain";
import {LambdaFunction} from "aws-cdk-lib/aws-events-targets";

export function createDynamoDB(stack:Construct): Table[] {
  const referralTable = createBaseTable("referral", stack)
  const usersTable = createBaseTable("users", stack)
  return [
    referralTable, usersTable
  ]
}

function createBaseTable(tableName:string, stack:Construct) : Table {
  const name = tableName
  return new Table(stack, name, {
    partitionKey: {
      name: "id",
      type: AttributeType.STRING
    },
    tableName: name
  })
}