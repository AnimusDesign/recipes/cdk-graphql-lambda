const tsconfig = require("./tsconfig.base.json")
const paths = tsconfig.compilerOptions.paths
const moduleNameMapper = Object.keys(paths).reduce((acc, curr) => {
  return {
    ...acc,
    [curr]: "<rootDir>" + "/../" + paths[curr]
  }
}, {})
module.exports = {
	  testMatch: ['**/+(*.)+(spec|test).+(ts|js)?(x)'],
	  preset: "ts-jest",
	  transform: {	
    "^.+\\.[tj]sx?$": "ts-jest"
  },
  moduleFileExtensions: ["ts", "tsx", "js", "jsx"],
  moduleNameMapper:   moduleNameMapper

}
