module.exports = {
  reporters: ["default", "jest-junit"],
  projects: [
	  "<rootDir>/backend",
	  "<rootDir>/frontend",
	  "<rootDir>/graphql/plugins-dynamo",
  ]
};

